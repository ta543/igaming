// Utility functions used across the application
function toggleVisibility(elementId) {
    const element = document.getElementById(elementId);
    element.style.display = (element.style.display === 'none' ? '' : 'none');
}
