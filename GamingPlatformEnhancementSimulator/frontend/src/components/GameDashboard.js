import React from 'react';

const GameDashboard = () => {
  return (
    <div>
      <h2>Game Dashboard</h2>
      <p>Manage your games and view real-time data here.</p>
    </div>
  );
}

export default GameDashboard;
