import React from 'react';

const Home = () => {
  return (
    <div>
      <h2>Welcome to the iGaming Platform</h2>
      <p>This is your one-stop hub for engaging and exciting games!</p>
    </div>
  );
}

export default Home;
