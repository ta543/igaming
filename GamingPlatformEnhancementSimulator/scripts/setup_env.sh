#!/bin/bash
echo "Setting up the development environment..."
cd /Users/chappy/chappy/Coding/Projects/free-time-projects/Project/Dev/igaming/GamingPlatformEnhancementSimulator/backend
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
cd /Users/chappy/chappy/Coding/Projects/free-time-projects/Project/Dev/igaming/GamingPlatformEnhancementSimulator/frontend
npm install
echo "Environment setup completed successfully."
