#!/bin/bash
set -e
echo "Starting deployment process..."
cd /Users/chappy/chappy/Coding/Projects/free-time-projects/Project/Dev/igaming/GamingPlatformEnhancementSimulator/backend
source venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py collectstatic --noinput
echo "Restarting backend server..."
cd /Users/chappy/chappy/Coding/Projects/free-time-projects/Project/Dev/igaming/GamingPlatformEnhancementSimulator/frontend
npm install
npm run build
echo "Restarting nginx to load changes..."
echo "Deployment completed successfully."
