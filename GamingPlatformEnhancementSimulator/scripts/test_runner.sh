#!/bin/bash
set -e
echo "Running backend tests..."
cd /Users/chappy/chappy/Coding/Projects/free-time-projects/Project/Dev/igaming/GamingPlatformEnhancementSimulator/backend
source venv/bin/activate
python manage.py test
echo "Running frontend tests..."
cd /Users/chappy/chappy/Coding/Projects/free-time-projects/Project/Dev/igaming/GamingPlatformEnhancementSimulator/frontend
npm test
echo "All tests completed successfully."
