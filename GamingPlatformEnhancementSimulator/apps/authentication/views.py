from django.http import HttpResponse
from .models import CustomUser

def user_profile(request, username):
    user = CustomUser.objects.get(username=username)
    return HttpResponse(f'User Profile - Username: {user.username}, Bio: {user.bio}')
