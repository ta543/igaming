from pyflink.datastream import StreamExecutionEnvironment
from pyflink.table import StreamTableEnvironment

def process_game_events():
    env = StreamExecutionEnvironment.get_execution_environment()
    env.set_parallelism(1)
    t_env = StreamTableEnvironment.create(env)

    t_env.execute_sql("""
        CREATE TABLE GameEvents (
            user_id STRING,
            event_type STRING,
            event_time TIMESTAMP(3),
            WATERMARK FOR event_time AS event_time - INTERVAL '5' SECOND
        ) WITH (
            'connector' = 'kafka',
            'topic' = 'game-events',
            'properties.bootstrap.servers' = 'localhost:9092',
            'format' = 'json'
        )
    """)

    t_env.execute_sql("""
        INSERT INTO OutputTable
        SELECT event_type, COUNT(user_id) AS user_counts
        FROM GameEvents
        GROUP BY event_type, TUMBLE(event_time, INTERVAL '1' HOUR)
    """)

if __name__ == "__main__":
    process_game_events()
