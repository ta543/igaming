from pulsar import Client

def publish_message():
    client = Client('pulsar://localhost:6650')
    producer = client.create_producer('persistent://public/default/game-events')

    try:
        producer.send(('Player1 scored a goal in Soccer Game').encode('utf-8'))
        print("Message published successfully.")
    finally:
        client.close()

if __name__ == "__main__":
    publish_message()
