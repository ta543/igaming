from functools import wraps
from django.http import HttpResponseForbidden

def user_is_game_admin(view_func):
    """ Decorator to check if the user is a game administrator. """
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden("You are not authorized to view this page")
        if not request.user.is_staff:  # Assuming 'is_staff' denotes an admin
            return HttpResponseForbidden("You need admin privileges to access this.")
        return view_func(request, *args, **kwargs)
    return _wrapped_view
