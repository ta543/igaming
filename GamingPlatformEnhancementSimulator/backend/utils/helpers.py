import string
import random

def generate_game_code(length=6):
    """ Generate a random code for a new game session. """
    letters = string.ascii_uppercase
    return ''.join(random.choice(letters) for i in range(length))

def send_notification(user, message):
    """ Simulate sending a notification to a user. """
    print(f"Notification sent to {user.username}: {message}")
