from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('backend.apps.authentication.urls')),
    path('gameplay/', include('backend.apps.gameplay.urls')),
    path('analytics/', include('backend.apps.analytics.urls')),
]
