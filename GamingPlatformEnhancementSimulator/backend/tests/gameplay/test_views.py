from django.test import TestCase, Client
from backend.apps.gameplay.models import Game

class GameViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        Game.objects.create(title="Chess", description="A classic board game", release_date="2021-01-01")

    def test_game_list_view(self):
        response = self.client.get('/gameplay/list/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Chess", response.content.decode())
