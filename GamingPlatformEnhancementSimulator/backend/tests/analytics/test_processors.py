from django.test import TestCase
from backend.apps.analytics.dashboard import generate_dashboard_data

class AnalyticsProcessorTest(TestCase):
    def test_dashboard_data_generation(self):
        data = generate_dashboard_data()
        self.assertIsNotNone(data)  # Assuming the function returns some data
        self.assertIsInstance(data, dict)  # Assuming data should be a dictionary
