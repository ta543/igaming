# Example processor function to simulate game data processing
def process_game_data(game_id):
    # This function would contain logic to process game data
    print(f'Processing data for game {game_id}')
