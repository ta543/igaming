from django.http import JsonResponse
from .models import Game

def game_list(request):
    games = Game.objects.all().values('title', 'description', 'release_date')
    return JsonResponse(list(games), safe=False)
