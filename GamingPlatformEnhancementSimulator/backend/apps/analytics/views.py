from django.http import JsonResponse
from .models import PlayerActivity

def activity_report(request):
    activities = PlayerActivity.objects.all().values('player__username', 'game__title', 'duration')
    return JsonResponse(list(activities), safe=False)
