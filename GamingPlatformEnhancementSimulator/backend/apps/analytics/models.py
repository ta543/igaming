from django.db import models

class PlayerActivity(models.Model):
    player = models.ForeignKey('authentication.CustomUser', on_delete=models.CASCADE)
    game = models.ForeignKey('gameplay.Game', on_delete=models.CASCADE)
    duration = models.IntegerField(help_text='Duration in minutes')

    def __str__(self):
        return f'{self.player.username} - {self.game.title}'
