from django.urls import path
from .views import activity_report

urlpatterns = [
    path('report/', activity_report, name='activity_report'),
]
