# 🎰 iGaming Platform 🎮

The iGaming Platform is a cutting-edge solution for managing and experiencing interactive gaming online. This platform integrates robust backend services with a dynamic frontend to provide a seamless gaming experience.

## 🌟 Features

- **User Authentication** 🔐: Secure login and registration system.
- **Game Management** 🎲: Admin interface to manage games, including adding, updating, and removing games.
- **Live Game Play** 🕹️: Real-time gaming with integrated chat and interactive features.
- **Analytics Dashboard** 📊: Real-time analytics on player engagement and game statistics.

## 🛠️ Technology Stack

- **Frontend**: React, HTML, CSS, JavaScript
- **Backend**: Django, Django REST Framework
- **Database**: PostgreSQL
- **Real-time Processing**: Apache Kafka, Apache Flink
- **Messaging**: Apache Pulsar

## 📁 Project Structure

- `frontend/`: Contains all frontend codebase.
  - `src/`: React source files.
  - `static/`: Static resources like CSS and JavaScript.
  - `templates/`: HTML templates.
- `backend/`: Contains all backend codebase.
  - `django_config/`: Django project configuration.
  - `apps/`: Django apps like authentication, gameplay.
  - `utils/`: Utility scripts and helpers.
- `data_processing/`: Scripts for handling data streaming and processing.
- `scripts/`: Utility scripts for deployment and environment setup.

## 🚀 Getting Started

### Prerequisites

- Python 3.8+
- Node.js 12+
- PostgreSQL 12+
- Apache Kafka
- Apache Flink

## 📜 License

This project is licensed under the MIT License - see the `LICENSE.md` file for details.
